import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
declare var $: any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  customOptions: OwlOptions = {
    autoplay: true,
    margin: 20,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    nav: false,
    smartSpeed: 700,
    animateOut: 'fadeOut',
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
  }

  title = 'disruptive-effects';

  constructor() { }

  async ngOnInit() {
  }

  ngAfterViewInit(): void {
   $('.banner-carousel-wrapper').on('click', function (){
     let none_autoplay = $('.disruptive-intro iframe').attr('src');
     let auto_play = none_autoplay + '?autoplay=1';

     $('.disruptive-intro').removeClass('d-none');
     $('.disruptive-intro iframe').attr('src', auto_play);
     $('.banner-carousel-wrapper').addClass('d-none');
   });

  }

}
